#include <iostream>
#include <fstream>
#include <random>

using namespace std;

#include <base/sample.h>
#include <base/solution.h>
#include <base/hillClimber.h>
#include <base/recuit.h>

#include <eval/basicEval.h>
#include <eval/smartEval.h>
#include <eval/basicBiobjEval.h>
#include <eval/smartBiobjEval.h>

int main(int argc, char ** argv) {    
    cout << "--------HillClimber--------" << endl;
    HillClimber hc;
    /**
     * paramètres du run : 
     * fichier utilisé
     * graine aléatoire
     * nombre d'évalutions
     */
    ofstream fichier;
    string const nomfichier("hcDonnee.csv");
    fichier.open(nomfichier.c_str());

    for(int i = 0; i < 30; i++){
        string fi = "dfa_4_"+to_string(i)+"_0.01_train-sample.json";
        string fi1 = "dfa_4_"+to_string(i)+"_0.05_train-sample.json";
        string fi2 = "dfa_4_"+to_string(i)+"_0.1_train-sample.json";
        string fi3 = "dfa_8_"+to_string(i)+"_0.01_train-sample.json";
        string fi4 = "dfa_8_"+to_string(i)+"_0.05_train-sample.json";
        string fi5 = "dfa_8_"+to_string(i)+"_0.1_train-sample.json";
        string fi6 = "dfa_16_"+to_string(i)+"_0.01_train-sample.json";
        string fi7 = "dfa_16_"+to_string(i)+"_0.05_train-sample.json";
        string fi8 = "dfa_16_"+to_string(i)+"_0.1_train-sample.json";
        string fi9 = "dfa_32_"+to_string(i)+"_0.01_train-sample.json";
        string fi10 = "dfa_32_"+to_string(i)+"_0.05_train-sample.json";
        string fi11 = "dfa_32_"+to_string(i)+"_0.1_train-sample.json";
        vector<double> rst;
        fichier << "NB DFA" << "|" << "FITNESS" << "|" << "NB EVAL" << endl;
        cout << fi << endl;
        fichier << fi << endl;
        fichier << 4 << "|" << hc.run(fi,i,1000) << "|" << 1000 << endl;
        fichier << 4 << "|" << hc.run(fi,i,10000) << "|" << 10000 << endl;
        fichier << 4 << "|" << hc.run(fi,i,50000) << "|" << 50000 << endl;
        cout << fi1 << endl;
        fichier << fi1 << endl;
        fichier << 4 << "|" << hc.run(fi1,i,1000) << "|" << 1000 << endl;
        fichier << 4 << "|" << hc.run(fi1,i,10000) << "|" << 10000 << endl;
        fichier << 4 << "|" << hc.run(fi1,i,50000) << "|" << 50000 << endl;
        cout << fi2 << endl;
        fichier << fi2 << endl;
        fichier << 4 << "|" << hc.run(fi2,i,1000) << "|" << 1000 << endl;
        fichier << 4 << "|" << hc.run(fi2,i,10000) << "|" << 10000 << endl;
        fichier << 4 << "|" << hc.run(fi2,i,50000) << "|" << 50000 << endl;
        cout << fi3 << endl;
        fichier << fi3 << endl;
        fichier << 8 << "|" << hc.run(fi3,i,1000) << "|" << 1000 << endl;
        fichier << 8 << "|" << hc.run(fi3,i,10000) << "|" << 10000 << endl;
        fichier << 8 << "|" << hc.run(fi3,i,50000) << "|" << 50000 << endl;
        cout << fi4 << endl;
        fichier << fi4 << endl;
        fichier << 8 << "|" << hc.run(fi4,i,1000) << "|" << 1000 << endl;
        fichier << 8 << "|" << hc.run(fi4,i,10000) << "|" << 10000 << endl;
        fichier << 8 << "|" << hc.run(fi4,i,50000) << "|" << 50000 << endl;
        cout << fi5 << endl;
        fichier << fi5 << endl;
        fichier << 8 << "|" << hc.run(fi5,i,1000) << "|" << 1000 << endl;
        fichier << 8 << "|" << hc.run(fi5,i,10000) << "|" << 10000 << endl;
        fichier << 8 << "|" << hc.run(fi5,i,50000) << "|" << 50000 << endl;
        cout << fi6 << endl;
        fichier << fi6 << endl;
        fichier << 16 << "|" << hc.run(fi6,i,1000) << "|" << 1000 << endl;
        fichier << 16 << "|" << hc.run(fi6,i,10000) << "|" << 10000 << endl;
        fichier << 16 << "|" << hc.run(fi6,i,50000) << "|" << 50000 << endl;
        cout << fi7 << endl;
        fichier << fi7 << endl;
        fichier << 16 << "|" << hc.run(fi7,i,1000) << "|" << 1000 << endl;
        fichier << 16 << "|" << hc.run(fi7,i,10000) << "|" << 10000 << endl;
        fichier << 16 << "|" << hc.run(fi7,i,50000) << "|" << 50000 << endl;
        cout << fi8 << endl;
        fichier << fi8 << endl;
        fichier << 16 << "|" << hc.run(fi8,i,1000) << "|" << 1000 << endl;
        fichier << 16 << "|" << hc.run(fi8,i,10000) << "|" << 10000 << endl;
        fichier << 16 << "|" << hc.run(fi8,i,50000) << "|" << 50000 << endl;
        cout << fi9 << endl;
        fichier << fi9 << endl;
        fichier << 32 << "|" << hc.run(fi9,i,1000) << "|" << 1000 << endl;
        fichier << 32 << "|" << hc.run(fi9,i,10000) << "|" << 10000 << endl;
        fichier << 32 << "|" << hc.run(fi9,i,50000) << "|" << 50000 << endl;
        cout << fi10 << endl;
        fichier << fi10 << endl;
        fichier << 32 << "|" << hc.run(fi10,i,1000) << "|" << 1000 << endl;
        fichier << 32 << "|" << hc.run(fi10,i,10000) << "|" << 10000 << endl;
        fichier << 32 << "|" << hc.run(fi10,i,50000) << "|" << 50000 << endl;
        cout << fi11 << endl;
        fichier << fi11 << endl;
        fichier << 32 << "|" << hc.run(fi11,i,1000) << "|" << 1000 << endl;
        fichier << 32 << "|" << hc.run(fi11,i,10000) << "|" << 10000 << endl;
        fichier << 32 << "|" << hc.run(fi11,i,50000) << "|" << 50000 << endl;
    }
    /*hc.run(argv[1], atoi(argv[2]), atoi(argv[3]));*/
    fichier.close();
    cout << endl;

    cout << "-----------Recuit-----------" << endl;
    Recuit rc;
    /**
     * paramètres du run : 
     * fichier utilisé
     * graine aléatoire
     * nombre d'évalutions
     * température initiale
     * palier
     * coefficient diminution température
     */
    string const nomfichier1("rc.csv");
    fichier.open(nomfichier1.c_str());

    for(int i = 0; i < 30; i++){
        string fi = "dfa_4_"+to_string(i)+"_0.01_train-sample.json";
        string fi1 = "dfa_4_"+to_string(i)+"_0.05_train-sample.json";
        string fi2 = "dfa_4_"+to_string(i)+"_0.1_train-sample.json";
        string fi3 = "dfa_8_"+to_string(i)+"_0.01_train-sample.json";
        string fi4 = "dfa_8_"+to_string(i)+"_0.05_train-sample.json";
        string fi5 = "dfa_8_"+to_string(i)+"_0.1_train-sample.json";
        string fi6 = "dfa_16_"+to_string(i)+"_0.01_train-sample.json";
        string fi7 = "dfa_16_"+to_string(i)+"_0.05_train-sample.json";
        string fi8 = "dfa_16_"+to_string(i)+"_0.1_train-sample.json";
        string fi9 = "dfa_32_"+to_string(i)+"_0.01_train-sample.json";
        string fi10 = "dfa_32_"+to_string(i)+"_0.05_train-sample.json";
        string fi11 = "dfa_32_"+to_string(i)+"_0.1_train-sample.json";
        vector<double> rst;
        fichier << "NB DFA" << "|" << "FITNESS" << "|" << "NB EVAL" << "|" << "TEMPERATURE INITIAL" << "|" << "PALIER" << "|" << "COEFICIENT" << endl;
        cout << fi << endl;
        fichier << fi << endl;
        fichier << 4 << "|" << rc.run(fi,i,1000,1,10,0.95) << "|" << 1000 << "|" << 1 << "|" << 10 << "|" << 0.95 << endl;
        fichier << 4 << "|" << rc.run(fi,i,10000,1,100,0.95) << "|" << 10000 << "|" << 1 << "|" << 100 << "|" << 0.95 << endl;
        fichier << 4 << "|" << rc.run(fi,i,50000,1,100,0.95) << "|" << 50000 << "|" << 1 << "|" << 100 << "|" << 0.95 << endl;
        cout << fi1 << endl;
        fichier << fi1 << endl;
        fichier << 4 << "|" << rc.run(fi1,i,1000,1,10,0.95) << "|" << 1000 << "|" << 1 << "|" << 10 << "|" << 0.95 << endl;
        fichier << 4 << "|" << rc.run(fi1,i,10000,1,100,0.95) << "|" << 10000 << "|" << 1 << "|" << 100 << "|" << 0.95 << endl;
        fichier << 4 << "|" << rc.run(fi1,i,50000,1,100,0.95) << "|" << 50000 << "|" << 1 << "|" << 100 << "|" << 0.95 << endl;
        cout << fi2 << endl;
        fichier << fi2 << endl;
        fichier << 4 << "|" << rc.run(fi2,i,1000,1,10,0.95) << "|" << 1000 << "|" << 1 << "|" << 10 << "|" << 0.95 << endl;
        fichier << 4 << "|" << rc.run(fi2,i,10000,1,100,0.95) << "|" << 10000 << "|" << 1 << "|" << 100 << "|" << 0.95 << endl;
        fichier << 4 << "|" << rc.run(fi2,i,50000,1,100,0.95) << "|" << 50000 << "|" << 1 << "|" << 100 << "|" << 0.95 << endl;
        cout << fi3 << endl;
        fichier << fi3 << endl;
        fichier << 8 << "|" << rc.run(fi3,i,1000,1,10,0.95) << "|" << 1000 << "|" << 1 << "|" << 10 << "|" << 0.95 << endl;
        fichier << 8 << "|" << rc.run(fi3,i,10000,1,100,0.95) << "|" << 10000 << "|" << 1 << "|" << 100 << "|" << 0.95 << endl;
        fichier << 8 << "|" << rc.run(fi3,i,50000,1,100,0.95) << "|" << 50000 << "|" << 1 << "|" << 100 << "|" << 0.95 << endl;
        cout << fi4 << endl;
        fichier << fi4 << endl;
        fichier << 8 << "|" << rc.run(fi4,i,1000,1,10,0.95) << "|" << 1000 << "|" << 1 << "|" << 10 << "|" << 0.95 << endl;
        fichier << 8 << "|" << rc.run(fi4,i,10000,1,100,0.95) << "|" << 10000 << "|" << 1 << "|" << 100 << "|" << 0.95 << endl;
        fichier << 8 << "|" << rc.run(fi4,i,50000,1,100,0.95) << "|" << 50000 << "|" << 1 << "|" << 100 << "|" << 0.95 << endl;
        cout << fi5 << endl;
        fichier << fi5 << endl;
        fichier << 8 << "|" << rc.run(fi5,i,1000,1,10,0.95) << "|" << 1000 << "|" << 1 << "|" << 10 << "|" << 0.95 << endl;
        fichier << 8 << "|" << rc.run(fi5,i,10000,1,100,0.95) << "|" << 10000 << "|" << 1 << "|" << 100 << "|" << 0.95 << endl;
        fichier << 8 << "|" << rc.run(fi5,i,50000,1,100,0.95) << "|" << 50000 << "|" << 1 << "|" << 100 << "|" << 0.95 << endl;
        cout << fi6 << endl;
        fichier << fi6 << endl;
        fichier << 16 << "|" << rc.run(fi6,i,1000,1,10,0.95) << "|" << 1000 << "|" << 1 << "|" << 10 << "|" << 0.95 << endl;
        fichier << 16 << "|" << rc.run(fi6,i,10000,1,100,0.95) << "|" << 10000 << "|" << 1 << "|" << 100 << "|" << 0.95 << endl;
        fichier << 16 << "|" << rc.run(fi6,i,50000,1,100,0.95) << "|" << 50000 << "|" << 1 << "|" << 100 << "|" << 0.95 << endl;
        cout << fi7 << endl;
        fichier << fi7 << endl;
        fichier << 16 << "|" << rc.run(fi7,i,1000,1,10,0.95) << "|" << 1000 << "|" << 1 << "|" << 10 << "|" << 0.95 << endl;
        fichier << 16 << "|" << rc.run(fi7,i,10000,1,100,0.95) << "|" << 10000 << "|" << 1 << "|" << 100 << "|" << 0.95 << endl;
        fichier << 16 << "|" << rc.run(fi7,i,50000,1,100,0.95) << "|" << 50000 << "|" << 1 << "|" << 100 << "|" << 0.95 << endl;
        cout << fi8 << endl;
        fichier << fi8 << endl;
        fichier << 16 << "|" << rc.run(fi8,i,1000,1,10,0.95) << "|" << 1000 << "|" << 1 << "|" << 10 << "|" << 0.95 << endl;
        fichier << 16 << "|" << rc.run(fi8,i,10000,1,100,0.95) << "|" << 10000 << "|" << 1 << "|" << 100 << "|" << 0.95 << endl;
        fichier << 16 << "|" << rc.run(fi8,i,50000,1,100,0.95) << "|" << 50000 << "|" << 1 << "|" << 100 << "|" << 0.95 << endl;
        cout << fi9 << endl;
        fichier << fi9 << endl;
        fichier << 32 << "|" << rc.run(fi9,i,1000,1,10,0.95) << "|" << 1000 << "|" << 1 << "|" << 10 << "|" << 0.95 << endl;
        fichier << 32 << "|" << rc.run(fi9,i,10000,1,100,0.95) << "|" << 10000 << "|" << 1 << "|" << 100 << "|" << 0.95 << endl;
        fichier << 32 << "|" << rc.run(fi9,i,50000,1,100,0.95) << "|" << 50000 << "|" << 1 << "|" << 100 << "|" << 0.95 << endl;
        cout << fi10 << endl;
        fichier << fi10 << endl;
        fichier << 32 << "|" << rc.run(fi10,i,1000,1,10,0.95) << "|" << 1000 << "|" << 1 << "|" << 10 << "|" << 0.95 << endl;
        fichier << 32 << "|" << rc.run(fi10,i,10000,1,100,0.95) << "|" << 10000 << "|" << 1 << "|" << 100 << "|" << 0.95 << endl;
        fichier << 32 << "|" << rc.run(fi10,i,50000,1,100,0.95) << "|" << 50000 << "|" << 1 << "|" << 100 << "|" << 0.95 << endl;
        cout << fi11 << endl;
        fichier << fi11 << endl;
        fichier << 32 << "|" << rc.run(fi11,i,1000,1,10,0.95) << "|" << 1000 << "|" << 1 << "|" << 10 << "|" << 0.95 << endl;
        fichier << 32 << "|" << rc.run(fi11,i,10000,1,100,0.95) << "|" << 10000 << "|" << 1 << "|" << 100 << "|" << 0.95 << endl;
        fichier << 32 << "|" << rc.run(fi11,i,50000,1,100,0.95) << "|" << 50000 << "|" << 1 << "|" << 100 << "|" << 0.95 << endl;
    }
    fichier.close();
    /*rc.run(argv[1], atoi(argv[2]), atoi(argv[3]), stod(argv[4]), atoi(argv[5]), stod(argv[6]));*/
}