/*
  basicBiobjEval.h

 Author: 
  Sebastien Verel, 
  Univ. du Littoral Côte d'Opale, France.
 
 Date:
   2019/05/02 : Version 0
*/

#ifndef _basicBiobjEval_h
#define _basicBiobjEval_h

#include <base/solution.h>
#include <base/sample.h>
#include <eval/eval.h>

/*
    Evaluation function of DFA solution:
      Execute the DFA on the sample, 
      The fitness is a pair :
         - the ratio of words from the sample which are corrected computed
         - number of states used

*/
class BasicBiobjEval : public Eval< std::pair<double, unsigned> > {
public:
    typedef std::pair<double, unsigned> Fitness;

    using Eval::sample;

    BasicBiobjEval(Sample & _sample) : Eval(_sample) {
    }

    virtual void operator()(Solution<Fitness> & _solution) {
      unsigned correct = 0;
      std::vector<unsigned> ni(_solution.nStates, 0);

      bool response;
      for(auto word : sample.set) {
        if (_solution.accept(word, ni) == word.accept) {
          correct++;
        }
      }

      unsigned used = 0;
      for(auto i : ni) {
        if (i != 0)
          used++;
      }

      _solution.fitness(std::pair<double, unsigned>(((double) correct) / (double) sample.set.size(), used));
    }

};


#endif