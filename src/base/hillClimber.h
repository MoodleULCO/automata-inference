#ifndef _HILLCLIMBER_H
#define _HILLCLIMBER_H

using namespace std;

#include <iostream>
#include <fstream>
#include <random>
#include <vector>
#include <sstream>
#include <utility>

#include <base/sample.h>
#include <base/solution.h>

#include <eval/smartEval.h>

#include <rapidjson/document.h>

class HillClimber : public DFA 
{
public:

    HillClimber(){}

    vector<string> subChar(string str){
        vector<string> sub;
        istringstream strStream(str);

        for(string element; std::getline(strStream, element, '_'); ){
            sub.push_back(move(element));
        }

        return sub;
    }

    double run(string file, int gr, int _nbEvalMax){
        _file = file;
        string url = "/home/belywin/automata-inference/instances/" + _file;
        Sample sample(url.c_str());

        int nbEval = 0;
        int state = stoi(subChar(file)[1]);

        Solution<double> sol(state, 2);
        std::mt19937 graine(gr);
        std::uniform_int_distribution<int> dist(0,state-1);     
        std::uniform_int_distribution<int> distribution(1,(state-1));
        
        for (int i = 0; i < sol.function.size(); ++i) {
            for (int j = 0; j < sol.function[i].size(); ++j) {
                sol.function[i][j] = dist(graine);
            }
        }

        SmartEval eval(graine, sample);

        eval(sol);
    
        double fit = sol.fitness();
        double fitMax;
        bool fin = false;

        while(nbEval < _nbEvalMax && fin == false){
            fitMax = fit;

            for(int i = 0; i < state; ++i){
                for(int j = 0; j < 2; ++j){
                    sol.function[i][j] =  (sol.function[i][j] + distribution(graine) ) % state;
                    eval(sol);
                    nbEval++;

                    if(fitMax < sol.fitness()){
                        fitMax = sol.fitness();
                    }
                }
            }

            sol.fitness(fit);

            if (fitMax > sol.fitness()) {
                sol.fitness(fitMax);
            }else{
                fin = true;
            }
        }
        cout << "Solution : " << sol.fitness() << endl;
        return sol.fitness();
    }

    private:
        string _file;
};

#endif