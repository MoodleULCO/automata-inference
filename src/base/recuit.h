#ifndef _RECUIT_H
#define _RECUIT_H

using namespace std;

#include <iostream>
#include <fstream>
#include <random>
#include <vector>
#include <sstream>
#include <utility>
#include <math.h>

#include <base/sample.h>
#include <base/solution.h>

#include <eval/smartEval.h>

#include <rapidjson/document.h>

class Recuit : public DFA 
{
public:

    Recuit(){}

    vector<string> subChar(string str){
        vector<string> sub;
        istringstream strStream(str);

        for(string element; std::getline(strStream, element, '_'); ){
            sub.push_back(move(element));
        }

        return sub;
    }

    double run(string file, int gr, int _nbEvalMax, double temp, int pal, double ind){
        int palInit = pal;
        _file = file;
        string url = "/home/belywin/automata-inference/instances/" + _file;
        Sample sample(url.c_str());

        int nbEval = 0;
        int state = stoi(subChar(file)[1]);

        Solution<double> sol(state, 2);
        std::mt19937 graine(gr);
        std::uniform_int_distribution<int> dist(0,state-1);     
        std::uniform_int_distribution<int> distribution(1,(state-1));
        
        for (int i = 0; i < sol.function.size(); ++i) {
            for (int j = 0; j < sol.function[i].size(); ++j) {
                sol.function[i][j] = dist(graine);
            }
        }

        SmartEval eval(graine, sample);

        eval(sol);
    
        double fit;
        double fitMax;
        bool fin = false;

        while(nbEval < _nbEvalMax && fin == false){
            int solpre;
            int i = rand() % (state);
            int j = rand() % 2;

            solpre = sol.function[i][j];
            fit = sol.fitness();
            sol.function[i][j] =  (sol.function[i][j] + distribution(graine) ) % state;
            eval(sol);

            if(sol.fitness() - fit > 0){
                nbEval++;
                pal--;
            }else{
                if((double)rand()/((double)RAND_MAX+1) < exp((sol.fitness() - fit)/temp)){
                }
                else{
                    sol.function[i][j] = solpre;
                    sol.fitness(fit);
                }
                pal--;
                nbEval++;
            }
            if(pal == 0){
                temp = temp*ind;
                pal = palInit;
            }
        }
        cout << "Solution : " << sol.fitness() << endl;
        cout << "Temperature finale : " << temp << endl;
        return sol.fitness();
    }

    private:
        string _file;
};

#endif